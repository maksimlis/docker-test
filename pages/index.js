import React from 'react'
import Link from 'next/link'

const Home = () => (
	<>
		<Link href="/mem">
			<a>Mem page</a>
		</Link>
		<h1>Beer Page updated 2.0v</h1>
		<img src="/static/beer.jpg" />
	</>
);

export default Home
