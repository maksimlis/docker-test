FROM node:alpine AS base
WORKDIR /app

COPY ./package*.json ./

RUN yarn install --production

COPY . .

RUN yarn build

# CMD [ "npm", "run", "start" ]
CMD ["yarn", "start"]

FROM nginx:alpine AS static
RUN rm /etc/nginx/conf.d/*
COPY nginx/default.conf /etc/nginx/conf.d/


# FROM node:12.13.0-alpine AS base
# WORKDIR /app

# FROM base AS dependencies
# WORKDIR /app
# COPY package.json ./
# RUN yarn install

# FROM base as pure
# WORKDIR /app
# COPY package.json ./
# RUN yarn install --production

# FROM dependencies AS build
# WORKDIR /app
# COPY . .
# RUN yarn build

# FROM pure AS app
# WORKDIR /app
# COPY --from=build /app/.next ./.next/
# COPY public ./public/
# CMD ["yarn", "start"]
